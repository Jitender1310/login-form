export default class User {
    date: number;
    month: string;
    company_desc: string;
    location: string;
    location_receive: string;
    company_receive: string;
    transaction: string;
    state_accord: string;
    state_executed: string;
  }